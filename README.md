# Syncall

This is a very simple shell script to execute the `stow` command  recursively in a specific folder, creating symlinks from every sub-directory to the first level to a specific predetrmined path.

---

---
## Idea and usage

The idea for this script comes from the necessity of better organizing the directory added to the `$PATH` variable in my `.bashrc` file. I used to throw all the scripts in there, so that they can be called from every directory in the command line. This metod worked just fine, if it wasn't that at some point I felt the necessity of making some order inside my script folder. I wanted to create various subfolders, diveded by programming language, and in each of those another level of subfolders containing each one a project or a script, to be used also as a git repo.  

Unfortunally the recursion isn't permitted in folders added to the `$PATH` variable, and I would had to add each folder individually... not very convenient. And that's where `stow` comes into play.

### What is `stow`?
From the `stow` man pages:
> Stow is a symlink farm manager which takes distinct sets of software
       and/or data located in separate directories on the filesystem, and
       makes them all appear to be installed in a single directory tree.
       
So I created another directory, added it to `$PATH` and created symlinks there with `stow` from my script subfolders... that was better, but still not good enough. In fact, that could also be annoying if the project folders are many, and the symlinks must be refreshed each time a new scripts is added.

This script resolves that problem: it syncs the whole conent of the folder, creating symlinks from every subfolder recursively to the first level.

---

## Configuration

To use this script you have to change the target path in the `recursively_stow` function. For example, if you wish to link your files to a folder called `Links` in the home directory, you will have to change line 5:  
  
  ```bash
  cd "$D" && ls | xargs stow -t ~/Links;
  ```
  
The same thing must be done for the starting directory, that is the directory containing the folders that you want to sync.  
Let's say that you want to sync the content from a directory called `Base` situeted in your `Documents` folder, you will change line 10 to fit your needs:

```bash
cd ~/Documents/Base/
```

And that's it. Every time you add content to your `Base` directory, including subfolder to the first level, you don't need to do anything but call `syncall`, and it will sync everything for you. Convenient, isn't it?

---
## Dependencies

Obviously you have to have `stow` installed on your system for this to work. It should be in the official repos of your distribution.